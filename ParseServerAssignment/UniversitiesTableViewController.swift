//
//  UniversitiesTableViewController.swift
//  ParseServerAssignment
//
//  Created by Douglas Sass on 3/17/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

import UIKit
import Parse

class UniversitiesTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        let addFavoriteBtn = UIBarButtonItem(title: "Add Favorite", style: .plain, target: self, action: #selector(UniversitiesTableViewController.addFavoriteTouched))
        self.navigationItem.rightBarButtonItem  = addFavoriteBtn
        
        let logOutBtn = UIBarButtonItem(title: "Log Out", style: .plain, target: self, action: #selector(UniversitiesTableViewController.logOutTouched))
        self.navigationItem.leftBarButtonItem  = logOutBtn
    }
    
    func addFavoriteTouched() -> Void
    {
        let vc = AddFavViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func logOutTouched() -> Void
    {
        PFUser.logOut()
        let vc = ViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 10
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell!
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        
        let query = PFQuery(className:"university")
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            
            let object = (objects?[indexPath.row])! as PFObject
            cell?.textLabel?.text = object["universityName"] as? String
            
//            if error == nil {
//                
//                if var objects = objects {
//                    for object in objects {
//                        object = (objects?[indexPath.row])! as PFObject
//                        cell?.textLabel?.text = object["universityName"] as? String
//                    }
//                }
//            } else {
//                // Log details of the failure
//                print("Error: \(error!)")
//            }
        }

        return cell!
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

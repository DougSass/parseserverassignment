//
//  AddFavViewController.swift
//  ParseServerAssignment
//
//  Created by Douglas Sass on 3/17/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

import UIKit
import Parse

class AddFavViewController: UIViewController {
    
    var addUniversity = UITextField()
    var addUniveristyLbl = UILabel()
    
    var saveUniversityBtn = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.white
        
        //Text field for the favorite university to be entered
        addUniversity = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.size.width - 20, height: 45))
        addUniversity.placeholder = "University"
        addUniversity.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(addUniversity)
        
        //Label for univeristy
        addUniveristyLbl = UILabel(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 20, height: 45))
        addUniveristyLbl.text = "Enter Favorite University Below"
        addUniveristyLbl.textAlignment = NSTextAlignment.center
        addUniveristyLbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(addUniveristyLbl)
        
        //Button to save favorite university
        let saveUniversityBtn = UIButton(type: UIButtonType.system)
        saveUniversityBtn.frame = CGRect(x: 10, y: 225, width: self.view.frame.size.width - 20, height: 45)
        saveUniversityBtn.setTitle("Save Univeristy", for: UIControlState.normal)
        saveUniversityBtn.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(saveUniversityBtn)
        saveUniversityBtn.addTarget(self, action: #selector(saveUniversityBtnTouched(btn:)), for: UIControlEvents.touchUpInside)

        // Do any additional setup after loading the view.
    }
    
    func saveUniversityBtnTouched(btn:UIButton) -> Void
    {
        let user = PFUser.current()
        
        let obj = PFObject(className: "university")
        obj["universityName"] = addUniversity.text
        obj["username"] = user?.username
        obj.saveInBackground { (success, error) in
           print("made it")
        }
        //Push to tableview
        let vc = UniversitiesTableViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
